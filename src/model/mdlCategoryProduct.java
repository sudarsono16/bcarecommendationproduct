package model;

import com.google.gson.annotations.SerializedName;

public class mdlCategoryProduct {
    @SerializedName(value = "company-code", alternate = { "CompanyCode", "company_code" })
    public String CompanyCode;
    @SerializedName(value = "company-name", alternate = { "CompanyName", "company_name" })
    public String CompanyName;
}