package model;

import com.google.gson.annotations.SerializedName;

public class mdlAPIResult {
    @SerializedName(value = "error-schema", alternate = {"ErrorSchema","error_schema"})
    public model.mdlErrorSchema ErrorSchema;
    @SerializedName(value = "output-schema", alternate = {"OutputSchema","output_schema"})
    public Object OutputSchema;
}