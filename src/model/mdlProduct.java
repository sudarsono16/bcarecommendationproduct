package model;

import com.google.gson.annotations.SerializedName;

public class mdlProduct {
    @SerializedName(value="company-code", alternate= {"CompanyCode","company_code"})
    public String CompanyCode;
    
    @SerializedName(value="company-name", alternate= {"CompanyName","company_name"})
    public String CompanyName;
    
    @SerializedName(value="product-code", alternate= {"ProductCode","product_code"})
    public String ProductCode;
    
    @SerializedName(value="product-name", alternate= {"ProductName","product_name"})
    public String ProductName;
    
    @SerializedName(value="campaign-code", alternate= {"CampaignCode","campaign_code"})
    public String CampaignCode;
    
    @SerializedName(value="campaign-name", alternate= {"CampaignName","campaign_name"})
    public String CampaignName;
    
    @SerializedName(value="product-priority", alternate= {"ProductPriority","product_priority_number"})
    public String ProductPriority;
    
    @SerializedName(value="product-link", alternate= {"ProductLink","product_link"})
    public model.mdlProductLink ProductLink;
    
    @SerializedName(value="last-product", alternate= {"LastProduct","last_product"})
    public model.mdlLastProduct LastProduct;
}