package model;

import com.google.gson.annotations.SerializedName;

public class mdlLastProduct {
    @SerializedName(value = "product-code", alternate = { "ProductCode", "product_code" })
    public String ProductCode;
    @SerializedName(value = "referral-date", alternate = { "ReferralDate", "referral_date" })
    public String ReferralDate;
}