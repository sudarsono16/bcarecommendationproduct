package model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class mdlRecommendationProduct {

    @SerializedName(value = "cin", alternate = "CIN")
    public String CIN;

    @SerializedName(value = "recommendation-product", alternate = { "RecommendationProduct", "recommendation_product" })
    public List<model.mdlProduct> RecommendationProduct;

    @SerializedName(value = "category-product", alternate = { "CategoryProduct", "category_product" })
    public List<model.mdlCategoryProduct> CategoryProduct;
}
