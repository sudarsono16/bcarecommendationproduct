package model;

import com.google.gson.annotations.SerializedName;

public class mdlProductLink {
    @SerializedName(value = "url-mybca", alternate = { "UrlMybca", "url_mybca" })
    String UrlMybca;
    @SerializedName(value = "url-bcacoid", alternate = { "UrlBcacoid", "url_bcacoid" })
    String UrlBcacoid;
    @SerializedName(value = "url-futurebranch", alternate = { "UrlFuturebranch", "url_futurebranch" })
    String UrlFuturebranch;
    @SerializedName(value = "url-futurebranch-icon", alternate = { "UrlFuturebranchIcon", "url_futurebranch_icon" })
    String UrlFuturebranchIcon;
    @SerializedName(value = "url-futurebranch-ads", alternate = { "UrlFuturebranchAds", "url_futurebranch_advertisement" })
    String UrlFuturebranchAds;
}