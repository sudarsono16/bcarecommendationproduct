package model;

import com.google.gson.annotations.SerializedName;

public class mdlErrorSchema {
    @SerializedName(value="error-code", alternate= {"ErrorCode","error_code"})
    public String ErrorCode;
    @SerializedName(value="error-message", alternate= {"ErrorMessage","error_message"})
    public model.mdlMessage ErrorMessage;
}
