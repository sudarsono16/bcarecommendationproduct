package model;

import com.google.gson.annotations.SerializedName;

public class mdlMessage {
    @SerializedName(value = "indonesian", alternate = "Indonesian")
    public String Indonesian;
    @SerializedName(value = "english", alternate = "English")
    public String English;
}